/*
 * This file was generated by qdbusxml2cpp version 0.7
 * Command line was: qdbusxml2cpp -c DBusBaseServer -a dbusbaseserver.h:dbusbaseserver.cpp ../../SpaceMeldCommon/dbusbase.xml
 *
 * qdbusxml2cpp is Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#include "dbusbaseserver.h"
#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

/*
 * Implementation of adaptor class DBusBaseServer
 */

DBusBaseServer::DBusBaseServer(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    // constructor
    setAutoRelaySignals(true);
}

DBusBaseServer::~DBusBaseServer()
{
    // destructor
}

uchar DBusBaseServer::getButtonCount()
{
    // handle method call org.spacemeld.device.getButtonCount
    uchar response;
    QMetaObject::invokeMethod(parent(), "getButtonCount", Q_RETURN_ARG(uchar, response));
    return response;
}

QString DBusBaseServer::getDeviceName()
{
    // handle method call org.spacemeld.device.getDeviceName
    QString response;
    QMetaObject::invokeMethod(parent(), "getDeviceName", Q_RETURN_ARG(QString, response));
    return response;
}

QString DBusBaseServer::getInterfaceName()
{
    // handle method call org.spacemeld.device.getInterfaceName
    QString response;
    QMetaObject::invokeMethod(parent(), "getInterfaceName", Q_RETURN_ARG(QString, response));
    return response;
}

QString DBusBaseServer::getPath()
{
    // handle method call org.spacemeld.device.getPath
    QString response;
    QMetaObject::invokeMethod(parent(), "getPath", Q_RETURN_ARG(QString, response));
    return response;
}

