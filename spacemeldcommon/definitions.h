/*
SpaceMeld is a driver/API for spaceballs, spacemice, spacenavigators etc..
Copyright (C) 2014 Thomas Anderson blobfish[at]gmx.com

This file is part of SpaceMeld.

SpaceMeld is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SpaceMeld is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SpaceMeld.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define INTERFACE_STRING "Interfaces"
#define INTERFACE_STRING_SERIAL "Serial"
#define INTERFACE_STRING_USB "USB"
#define INTERFACE_STRING_JOYSTICK "Joystick"

#define ORG_NAME_STRING "Blobfish"
#define APP_NAME_STRING "SpaceMeld"

#define SERVICE_NAME_STRING "Space Meld"
#define SERVICE_PATH_STRING "ServicePath"
#define SERVICE_STATUS_STRING "DriverStatus"

#define DEVICES_STRING "Devices"
#define DEVICE_ID_STRING "DeviceId"
#define DEVICE_RUNTIMEID_STRING "DeviceRuntimeId"
#define DEVICE_NAME_STRING "DeviceName"
#define DEVICE_INTERFACE_ID_STRING "DeviceInterfaceId"
#define DEVICE_PATH_STRING "DevicePath"
#define DEVICE_ENABLED_STRING "DeviceEnabled"
#define DEVICE_EXPORT_STRING "DeviceExport"
#define DEVICE_DETECTED_STRING "DeviceDetected"

#define EXPORT_TYPE_STRING "ExportType"
#define EXPORT_ENABLED_STRING "ExportEnabled"

#define AXES_MUTATE_STRING "AxesMutate"
#define AXES_INVERSE_STRING "AxesInverse"
#define AXES_SCALE_STRING "AxesScale"
#define AXES_MAP_STRING "AxesMap"

#define BUTTON_KEY_MAP_STRING "ButtonKeyMap"
#define BUTTON_STRING "Button"
#define KEY_STRING "Key"

#endif //DEFINITIONS_H
